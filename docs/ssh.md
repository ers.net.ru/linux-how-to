## SSH

### Set env

```shell
export SSH_SHELL_HOSTNAME=ya-prod-nat
export SSH_SHELL_USERNAME=zvooq
```

### Check env

```shell
echo $SSH_SHELL_HOSTNAME
echo $SSH_SHELL_USERNAME
```

### Create ssh-key

```shell
ssh-keygen -t rsa -b 4096 -C $SSH_SHELL_USERNAME@$SSH_SHELL_HOSTNAME -f ~/.ssh/$SSH_SHELL_USERNAME@$SSH_SHELL_HOSTNAME
```

### Copy key to remote host

```shell
ssh-copy-id -f -i ~/.ssh/$SSH_SHELL_USERNAME@$SSH_SHELL_HOSTNAME $SSH_SHELL_USERNAME@$SSH_SHELL_HOSTNAME
```

### Verbose connect

```shell
ssh -vvv $SSH_SHELL_USERNAME@$SSH_SHELL_HOSTNAME
```

