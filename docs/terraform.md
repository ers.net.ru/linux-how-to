Установка Terraform
===

[https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)

## Linux repository

Install the HashiCorp GPG key.
```shell
wget -e use_proxy=yes -e https_proxy=109.107.172.217:8118 -O- https://apt.releases.hashicorp.com/gpg | \
    gpg --dearmor | \
    sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
```

Add repository
```shell
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] \
    https://apt.releases.hashicorp.com $(lsb_release -cs) main" | \
    sudo tee /etc/apt/sources.list.d/hashicorp.list
```

[Включаем прокси для apt](proxy.md#apt)

```shell
sudo apt update
```

```shell
sudo apt-get install terraform
```

## Manual

[Downloads](https://developer.hashicorp.com/terraform/downloads)

```shell
wget -e use_proxy=yes -e https_proxy=109.107.172.217:8118 https://releases.hashicorp.com/terraform/1.3.6/terraform_1.3.6_linux_amd64.zip -O /tmp/terraform_linux_amd64.zip
```

```shell
sudo unzip /tmp/terraform_linux_amd64.zip -d /opt
```

```shell
sudo ln -s /opt/terraform /usr/local/bin
```


## Verify install
```shell
terraform --version
```

## Then install the autocomplete package.

```shell
terraform -install-autocomplete
```

	```shel
	sdfsdf
	
```
