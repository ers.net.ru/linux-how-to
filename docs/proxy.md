## Использование прокси в различных ссценариях

### Apt

```shell
echo 'Acquire::http::proxy "http://109.107.172.217:8118";' >> /etc/apt/apt.conf.d/30proxy
echo 'Acquire::ftp::proxy "ftp://109.107.172.217:8118";' >> /etc/apt/apt.conf.d/30proxy
echo 'Acquire::https::proxy "http://109.107.172.217:8118";' >> /etc/apt/apt.conf.d/30proxy
```