### Mount QCOW2 Disk Image on Debian/Ubuntu Linux

Install qemu-utils:

```shell
apt install qemu-utils
```

Load nbd kernel module:

```shell
modprobe nbd max_part=8
```
Mount QCOW2 disk image as NBD device:

```shell
qemu-nbd -c /dev/nbd0 --read-only <$PATH_TO_QCOW2>
```

List partitions on NBD device (optional):

```shell
fdisk -l /dev/nbd0
```

Mount NBD partition’s filesystem:

```shell
mount -o ro /dev/nbd0p1 /mnt
```

Unmount partition

```shell
umount /mnt
```

Disconnect drive

```shell
qemu-nbd --disconnect /dev/nbd0
```
