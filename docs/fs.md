```
# lsblk -o +MODEL,SERIAL
sblk -o +MODEL,SERIAL
NAME                         MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT MODEL                          SERIAL
sda                            8:0    0   30G  0 disk            Virtual_disk                   
├─sda1                         8:1    0 1007K  0 part                                           
├─sda2                         8:2    0  512M  0 part                                           
└─sda3                         8:3    0 29.5G  0 part                                           
  ├─pve-swap                 253:0    0  3.6G  0 lvm  [SWAP]                                    
  ├─pve-root                 253:1    0 12.9G  0 lvm  /                                         
  ├─pve-data_tmeta           253:2    0    1G  0 lvm                                            
  │ └─pve-data-tpool         253:4    0 10.9G  0 lvm                                            
  │   ├─pve-data             253:5    0 10.9G  1 lvm                                            
  │   └─pve-vm--100--disk--0 253:6    0   32G  0 lvm                                            
  └─pve-data_tdata           253:3    0 10.9G  0 lvm                                            
    └─pve-data-tpool         253:4    0 10.9G  0 lvm                                            
      ├─pve-data             253:5    0 10.9G  1 lvm                                            
      └─pve-vm--100--disk--0 253:6    0   32G  0 lvm                                            
sdb                            8:16   0   50G  0 disk            Virtual_disk                   
├─sdb1                         8:17   0   50G  0 part                                           
└─sdb9                         8:25   0    8M  0 part                                           
sdc                            8:32   0   50G  0 disk            Virtual_disk                   
├─sdc1                         8:33   0   50G  0 part                                           
└─sdc9                         8:41   0    8M  0 part                                           
sdd                            8:48   0   50G  0 disk            Virtual_disk                   
├─sdd1                         8:49   0   50G  0 part                                           
└─sdd9                         8:57   0    8M  0 part                                           
sde                            8:64   0   50G  0 disk            Virtual_disk                   
├─sde1                         8:65   0   50G  0 part                                           
└─sde9                         8:73   0    8M  0 part                                           
sr0                           11:0    1 1024M  0 rom             VMware_Virtual_IDE_CDROM_Drive 00000000000000000001
```

## Список дисков
```
# fdisk -l


Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: Virtual disk    
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 00EA1FA2-4844-409C-A00B-622E13A1686D

Device       Start      End  Sectors  Size Type
/dev/sda1       34     2047     2014 1007K BIOS boot
/dev/sda2     2048  1050623  1048576  512M EFI System
/dev/sda3  1050624 62914526 61863903 29.5G Linux LVM


Disk /dev/sdb: 50 GiB, 53687091200 bytes, 104857600 sectors
Disk model: Virtual disk    
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 2CDA101D-A81D-F840-9C86-D3343C1FAD59

Device         Start       End   Sectors Size Type
/dev/sdb1       2048 104839167 104837120  50G Solaris /usr & Apple ZFS
/dev/sdb9  104839168 104855551     16384   8M Solaris reserved 1


Disk /dev/sdc: 50 GiB, 53687091200 bytes, 104857600 sectors
Disk model: Virtual disk    
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 749B1137-3900-924C-8564-D362D4975F6D

Device         Start       End   Sectors Size Type
/dev/sdc1       2048 104839167 104837120  50G Solaris /usr & Apple ZFS
/dev/sdc9  104839168 104855551     16384   8M Solaris reserved 1


Disk /dev/sdd: 50 GiB, 53687091200 bytes, 104857600 sectors
Disk model: Virtual disk    
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: 3E91E023-1CB2-4A42-A6D6-0D55CFED9226

Device         Start       End   Sectors Size Type
/dev/sdd1       2048 104839167 104837120  50G Solaris /usr & Apple ZFS
/dev/sdd9  104839168 104855551     16384   8M Solaris reserved 1


Disk /dev/sde: 50 GiB, 53687091200 bytes, 104857600 sectors
Disk model: Virtual disk    
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: gpt
Disk identifier: E4B3AA89-6D45-1E4D-BC5A-E02CD2DFB5D9

Device         Start       End   Sectors Size Type
/dev/sde1       2048 104839167 104837120  50G Solaris /usr & Apple ZFS
/dev/sde9  104839168 104855551     16384   8M Solaris reserved 1


Disk /dev/mapper/pve-swap: 3.63 GiB, 3892314112 bytes, 7602176 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/pve-root: 12.93 GiB, 13887340544 bytes, 27123712 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes


Disk /dev/mapper/pve-vm--100--disk--0: 32 GiB, 34359738368 bytes, 67108864 sectors
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 65536 bytes / 65536 bytes
Disklabel type: gpt
Disk identifier: 4EA4104B-02BA-47BB-8D45-15DA0C7C0974

Device                                   Start      End  Sectors  Size Type
/dev/mapper/pve-vm--100--disk--0-part1    2048     4095     2048    1M BIOS boot
/dev/mapper/pve-vm--100--disk--0-part2    4096  1054719  1050624  513M EFI System
/dev/mapper/pve-vm--100--disk--0-part3 1054720 67106815 66052096 31.5G Linux LVM
```

### Display or manipulate a disk partition table

```
cfdisk /dev/sdb
```