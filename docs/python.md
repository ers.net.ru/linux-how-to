Create virtual environment

```shell
python3 -m venv ${ENV_NAME}
```

Activate environment

```shell
source ${ENV_NAME}/bin/activate
```

Deactivate environment
```shell
deactivate
```


Generate a `requirements.txt` file for your project

```shell
pip freeze > requirements.txt
```
