```shell
sudo apt install zsh
```

Установка по дефолту

```shell
chsh -s $(which zsh)
```

Oh-My-Zsh

```shell
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

### zsh-autosuggestions

Добавляет автозаполнения для shell-команд.

```shell
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
```

### zsh-syntax-highlighting

Подсветка синтаксиса в оболочке.

```shell
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

### zsh-ssh

Подключения из `ssh_config` (требуеет установленного `fzf`)

```shell
git clone https://github.com/sunlei/zsh-ssh ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-ssh
```

### power10k

```shell
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
```

Чтобы постоянно отображался k8s контекст, небходимо закомменитровать строку `typeset -g POWERLEVEL9K_KUBECONTEXT_SHOW_ON_COMMAND=...` в файле `~/.p10k.zsh`

### ktx kns

```shell
curl https://raw.githubusercontent.com/blendle/kns/master/bin/kns -o /.local/bin/kns && chmod +x $_
```