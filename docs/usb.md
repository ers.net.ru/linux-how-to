## USB 

### balenaEtcher

Flash OS images to SD cards & USB drives, safely and easily.

[balenaEtcher]https://www.balena.io/etcher/)


### Allow user all usb devices

```shell
sudo usermod -a -G dialout $USER
```
