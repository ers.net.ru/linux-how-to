Удаление пода в зависшем статусе `Terminating`

```shell
kubectl delete pod NAME --grace-period=0 --force    
```